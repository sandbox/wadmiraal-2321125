<?php

/**
 * @file
 * Defines a video upload field for using the Infomaniak VOD service.
 */

/**
 * @mainpage Infomaniak Video Field
 * The Infomaniak Video Field module exposes a custom CCK file field, that
 * allows users to upload videos to the Infomaniak VOD service.
 *
 * This is done through a custom field that extends the Filefield module, as
 * as a custom widget and a custom formatter.
 *
 * This module requires an account with Infomaniak for the VOD service. The
 * account is tied to a specific field, and is not platform-wide, allowing
 * admins to use different Infomaniak accounts for different purposes.
 *
 * Videos are processed on cron runs, and thus require a cron job to be set up
 * in order to work.
 *
 * The documentation for the VOD SOAP API can be found here:
 * https://statslive.infomaniak.com/vod/api/
 * Our own implementation of a SoapClient is in src/Infomaniak/VOD/SoapClient.
 * This class provides all necessary methods to communicate with the VOD
 * server.
 *
 * A callback URL for the VOD server is available at infomaniak/vod/callback.
 * This URL can be used by the VOD server to notify the module about events,
 * such as video uploads, conversion, etc. It is recommended to setup the VOD
 * to notify the module on the following events:
 * - Video deleted
 * - Encode finished
 * - Download OK
 *
 * For the other events, the module won't take action, but will simply log the
 * request.
 *
 * @see Infomaniak\VOD\SoapClient
 */

define('INFOMANIAK_VIDEO_FIELD__STATUS_PENDING', 'pending');
define('INFOMANIAK_VIDEO_FIELD__STATUS_QUEUED', 'queued');
define('INFOMANIAK_VIDEO_FIELD__STATUS_CONVERSION', 'conversion');
define('INFOMANIAK_VIDEO_FIELD__STATUS_READY', 'ready');
define('INFOMANIAK_VIDEO_FIELD__STATUS_ERROR', 'error');

/**
 * Implements hook_menu().
 */
function infomaniak_video_field_menu() {
  return array(
    'infomaniak/vod/callback' => array(
      'access callback' => TRUE,
      'page callback' => 'infomaniak_video_field_vod_callback',
      'file' => 'includes/infomaniak_video_field.pages.inc',
      'type' => MENU_CALLBACK,
    ),
  );
}

/**
 * Implements hook_theme().
 */
function infomaniak_video_field_theme() {
  return array(
    'infomaniak_video_field_widget' => array(
      'arguments' => array('element' => NULL),
      'file' => 'theme/theme.inc',
    ),
    'infomaniak_video_field_formatter_default' => array(
      'arguments' => array('element' => NULL),
      'file' => 'theme/theme.inc',
    ),
    'infomaniak_video_field_widget_preview' => array(
      'arguments' => array('file' => NULL, 'widget' => NULL, 'field' => NULL),
      'file' => 'theme/theme.inc',
    ),
    'infomaniak_video_field_player' => array(
      'arguments' => array('file' => NULL, 'widget' => NULL, 'field' => NULL, 'width' => NULL, 'height' => NULL),
      'file' => 'theme/theme.inc',
    ),
  );
}

/**
 * Implements hook_cron().
 */
function infomaniak_video_field_cron() {
  module_load_include('inc', 'infomaniak_video_field', 'includes/infomaniak_video_field.cron');

  return _infomaniak_video_field_cron();
}

/**
 * Implements hook_elements().
 */
function infomaniak_video_field_elements() {
  $elements = array();

  // Catch problems when this is called too early during installation or update.
  if (!module_exists('filefield')) {
    return $elements;
  }

  // An Infomaniak Video Field is really just a FileField with extra processing.
  $filefield_elements = module_invoke('filefield', 'elements');
  $elements['infomaniak_video_field_widget'] = $filefield_elements['filefield_widget'];
  $elements['infomaniak_video_field_widget']['#process'][] = 'infomaniak_video_field_widget_process';

  return $elements;
}

/**
 * Implements hook_field_info().
 */
function infomaniak_video_field_field_info() {
  return array(
    'infomaniak_video_field' => array(
      'label' => t('Infomaniak Video'),
      'description' => t('Store video files on an Infomaniak VOD Server.'),
    ),
  );
}

/**
 * Implements hook_field_settings().
 */
function infomaniak_video_field_field_settings($op, $field) {
  module_load_include('inc', 'infomaniak_video_field', 'includes/infomaniak_video_field.field');

  switch ($op) {
    case 'form':
      return infomaniak_video_field_field_settings_form($field);

    case 'validate':
      return infomaniak_video_field_field_settings_validate($field);

    case 'save':
      return infomaniak_video_field_field_settings_save($field);

    case 'database columns':
      return infomaniak_video_field_field_settings_database_columns($field);

    case 'views data':
      return infomaniak_video_field_field_settings_views_data($field);
  }
}

/**
 * Implements hook_field().
 */
function infomaniak_video_field_field($op, $node, $field, &$items, $teaser, $page) {
  module_load_include('inc', 'infomaniak_video_field', 'includes/infomaniak_video_field.field');

  switch ($op) {
    case 'load':
      return infomaniak_video_field_field_load($node, $field, $items, $teaser, $page);

    case 'insert':
      return infomaniak_video_field_field_insert($node, $field, $items, $teaser, $page);

    case 'update':
      return infomaniak_video_field_field_update($node, $field, $items, $teaser, $page);

    case 'delete':
      return infomaniak_video_field_field_delete($node, $field, $items, $teaser, $page);

    case 'delete revision':
      return infomaniak_video_field_field_delete_revision($node, $field, $items, $teaser, $page);

    case 'sanitize':
      return infomaniak_video_field_field_sanitize($node, $field, $items, $teaser, $page);
  }
}

/**
 * Implements hook_content_is_empty().
 */
function infomaniak_video_field_content_is_empty($item, $field) {
  return filefield_content_is_empty($item, $field);
}

/**
 * Implements hook_widget_info().
 */
function infomaniak_video_field_widget_info() {
  return array(
    'infomaniak_video_field_widget' => array(
      'label' => t('Infomaniak Video'),
      'field types' => array('infomaniak_video_field'),
      'multiple values' => CONTENT_HANDLE_CORE,
      'callbacks' => array('default value' => CONTENT_CALLBACK_CUSTOM),
      'description' => t('A widget for uploading and managing videos with the Infomaniak VOD service.'),
    ),
  );
}

/**
 * Implements hook_widget_settings().
 */
function infomaniak_video_field_widget_settings($op, $widget) {
  module_load_include('inc', 'infomaniak_video_field', 'includes/infomaniak_video_field.widget');

  switch ($op) {
    case 'form':
      return infomaniak_video_field_widget_settings_form($widget);

    case 'save':
      return infomaniak_video_field_widget_settings_save($widget);
  }
}

/**
 * Implements hook_widget().
 */
function infomaniak_video_field_widget(&$form, &$form_state, $field, $items, $delta = 0) {
  module_load_include('inc', 'infomaniak_video_field', 'includes/infomaniak_video_field.widget');

  return _infomaniak_video_field_widget($form, $form_state, $field, $items, $delta);
}

/**
 * Implements hook_field_formatter_info().
 */
function infomaniak_video_field_field_formatter_info() {
  return array(
    'default' => array(
      'label' => t("Infomaniak VOD Player (default)"),
      'field types' => array('infomaniak_video_field'),
      'description' => t("Displays a video player for the video, as configured inside the widget."),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
  );
}

/**
 * Implements hook_file_references().
 */
function infomaniak_video_field_file_references($file) {
  module_load_include('inc', 'infomaniak_video_field', 'includes/infomaniak_video_field.file');

  return _infomaniak_video_field_file_references($file);
}

/**
 * Implements hook_file_download().
 */
function infomaniak_video_field_file_download($filepath) {
  module_load_include('inc', 'infomaniak_video_field', 'includes/infomaniak_video_field.file');

  return _infomaniak_video_field_file_download($filepath);
}

/**
 * Process callback for our custom form element.
 */
function infomaniak_video_field_widget_process($element, $edit, &$form_state, $form) {
  module_load_include('inc', 'infomaniak_video_field', 'includes/infomaniak_video_field.widget');

  return _infomaniak_video_field_widget_process($element, $edit, $form_state, $form);
}

/**
 * Return a list of all existing Infomaniak Video fields.
 *
 * Lists all informaniak_video_fields across all node types. The result of this
 * call is statically cached. It is similar to filefield_get_field_list().
 *
 * @see filefield_get_field_list()
 *
 * @return array
 *   The list of all infomaniak_video_fields, keyed by field machine name.
 */
function infomaniak_video_field_get_field_list() {
  static $fields;

  if (!isset($fields)) {
    $fields = content_fields();

    // Filter down the list just to our fields.
    foreach ($fields as $key => $field) {
      if ($field['type'] != 'infomaniak_video_field') {
        unset($fields[$key]);
      }
    }
  }

  return $fields;
}

/**
 * Return a SOAP client for the passed account.
 *
 * Factory function that returns an instance for the passed VOD account.
 * This instance is statically cached, and can be reused.
 *
 * @param string $login
 *   The VOD account login.
 * @param string $password
 *   The VOD account password.
 * @param string $vod_id
 *   The VOD account ID.
 *
 * @return Infomaniak\VOD\SoapClient
 *   A SOAP client instance.
 */
function infomaniak_video_field_get_soap_client($login, $password, $vod_id) {
  static $clients = array();

  if (!isset($clients["$login:$vod_id"])) {
    module_load_include('php', 'infomaniak_video_field', 'src/Infomaniak/VOD/SoapClient');

    $clients["$login:$vod_id"] = new Infomaniak\VOD\SoapClient($login, $password, $vod_id);
  }

  return $clients["$login:$vod_id"];
}

/**
 * Get the (probable) file ID the video will receive on the VOD server.
 *
 * The VOD server is technically still very crude. One of the issues it has, is
 * it requires a video ID to manipulate or play videos from another service.
 * However, it does not provide the ID it gives a video when it has uploaded
 * it (even through the callback). This is stupid, of course, but has been the
 * situation for years. And probably won't change. So, the best we can do is
 * give it an id ourselves. It's not perfect (for instance, what if
 * a video with the same name was already uploaded by hand) but we have to.
 * At least, we can make it unique enough to make name collisions very unlikely.
 *
 * @param string $filepath
 *   The file path of the video.
 *
 * @return string
 *   The generated video ID
 */
function infomaniak_video_field_get_video_id($filepath) {
  $parts = @end(explode('/', $filepath));
  // Because those morons will sneakily remove the "extension" from the ID
  // mid way (from queue to download, they'll keep it - from encoding onwards,
  // they remove it), remove all '.' separators. Just use _ .
  $filename = str_replace('.', '_', $parts);

  return $filename . '_' . uniqid();
}

/**
 * Update the video status.
 *
 * As we don't know where the video is located (field and content type),
 * we loop over all fields and try to find it.
 *
 * @param string $video_id
 *   The video ID, which is unique across all videos on the platform.
 * @param string $status
 *   The new status for the video.
 */
function infomaniak_video_field_update_status($video_id, $status) {
  $fields = infomaniak_video_field_get_field_list();

  foreach ($fields as $field) {
    $db_info = content_database_info($field);

    $video_id_column = $db_info['columns']['infomaniak_vod_video_id']['column'];
    $status_column = $db_info['columns']['infomaniak_vod_status']['column'];
    $db_table = $db_info['table'];

    $result = db_query("
      SELECT  c.{$video_id_column}
        FROM  {{$db_table}} c
       WHERE  c.{$video_id_column} = '%s'
    ", $video_id);

    if ($result) {
      $row = db_fetch_array($result);

      if (!empty($row)) {
        db_query("
          UPDATE  {{$db_table}}
             SET  {$status_column} = '%s'
           WHERE  {$video_id_column} = '%s'
        ", array(
          $status,
          $video_id,
        ));

        // Break out of the loop. We're done.
        break;
      }
    }
  }
}

/**
 * Delete a file.
 *
 * This checks with the field settings of the video should also be deleted on
 * the VOD server. It uses field_file_delete() for performing the actual
 * deletion.
 *
 * @see field_file_delete()
 *
 * @param object $file
 *   The file to be deleted.
 * @param object $field
 *   The field referencing containing the file.
 */
function infomaniak_video_field_delete_file($file, $field) {
  // Forcibly remove the file from the system.
  field_file_delete($file, TRUE);

  if (!empty($field['infomaniak_vod_delete_video']) && !empty($file->infomaniak_vod_video_id)) {
    $client = infomaniak_video_field_get_soap_client(
      $field['infomaniak_vod_login'],
      $field['infomaniak_vod_password'],
      $field['infomaniak_vod_id']
    );

    $client->delete($file->infomaniak_vod_folder_id, $file->infomaniak_vod_video_id);
  }
}
