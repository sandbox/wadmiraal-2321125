<?php

/**
 * @file
 * Theme preprocessors and callbacks.
 */

/**
 * Theme callback for infomaniak_video_field.
 *
 * @ingroup themeable
 */
function theme_infomaniak_video_field_widget($element) {
  return '<div class="infomaniak-video-field-wrapper">' . theme('form_element', $element, $element['#children']) . '</div>';
}

/**
 * Theme callback for infomaniak_video_field_formatter_default.
 *
 * @ingroup themeable
 */
function theme_infomaniak_video_field_formatter_default($element) {
  $file = $element['#item'];

  if ($file['infomaniak_vod_status'] == INFOMANIAK_VIDEO_FIELD__STATUS_READY) {
    $field = content_fields($element['#field_name'], $element['#type_name']);

    return theme_infomaniak_video_field_player($file, $field['widget'], $field);
  }
  else {
    return '';
  }
}

/**
 * Theme callback for infomaniak_video_field_widget_preview.
 *
 * @ingroup themeable
 */
function theme_infomaniak_video_field_widget_preview($file, $widget, $field) {
  switch ($file['infomaniak_vod_status']) {
    case INFOMANIAK_VIDEO_FIELD__STATUS_ERROR:
      return '<div class="message error">' . t("An error occured with this video on the VOD server. Please contact an administrator. Video ID: @video_id", array('@video_id' => $file['infomaniak_vod_video_id'])) . '</div>';

    case INFOMANIAK_VIDEO_FIELD__STATUS_READY:
      return theme('infomaniak_video_field_player', $file, $widget, $field, 240, 180);

    case INFOMANIAK_VIDEO_FIELD__STATUS_CONVERSION:
      return theme_progress_bar(75, t("Being converted to output formats"));

    case INFOMANIAK_VIDEO_FIELD__STATUS_QUEUED:
      return theme_progress_bar(25, t("Queued for upload to Infomaniak"));

    case INFOMANIAK_VIDEO_FIELD__STATUS_PENDING:
    default:
      return theme_progress_bar(0, t("Pending transfer to Infomaniak"));

  }
}

/**
 * Theme callback for infomaniak_video_field_player.
 *
 * @ingroup themeable
 */
function theme_infomaniak_video_field_player($file, $widget, $field, $width = NULL, $height = NULL) {
  $client = infomaniak_video_field_get_soap_client(
    $field['infomaniak_vod_login'],
    $field['infomaniak_vod_password'],
    $field['infomaniak_vod_id']
  );

  try {
    return $client->getPlayerIFrame(
      $file['infomaniak_vod_folder_id'],
      $file['infomaniak_vod_video_id'],
      $widget['infomaniak_vod_player_id'],
      $width,
      $height
    );
  }
  catch(Exception $e) {
    return '<div class="message error">' . check_plain($e->getMessage()) . '</div>';
  }
}
