<?php

/**
 * @file
 * Actual CCK widget hook implementations, as well as widget specific logic.
 */

/**
 * Actual implementation of hook_widget_settings(), $op = 'form'.
 */
function infomaniak_video_field_widget_settings_form($widget) {
  $form = module_invoke('filefield', 'widget_settings', 'form', $widget);

  $menu_item = menu_get_item();
  $url_parts = explode('/', $menu_item['path']);
  // The URL is of the following format:
  // admin/content/node-type/[node type]/fields/[field name].
  // We need the field settings themselves, because the widget depends on it.
  // However, there's no easy way to get the field. So we look at the
  // information in the URL and load the field settings.
  $field = content_fields($url_parts[5], $url_parts[3]);

  if (!empty($field['infomaniak_vod_login'])) {
    $client = infomaniak_video_field_get_soap_client(
      $field['infomaniak_vod_login'],
      $field['infomaniak_vod_password'],
      $field['infomaniak_vod_id']
    );

    // @todo - provide some hierarchy information ?
    foreach ($client->getFolders() as $folder) {
      if ($folder['sFolderPath']) {
        $folders[$folder['sFolderPath']] = $folder['sFolderName'];
      }
    }

    foreach ($client->getPlayers() as $player) {
      $players[$player['iPlayerCode']] = $player['sName'] . "({$player['iWidth']}x{$player['iHeight']})";
    }
  }
  else {
    $folders = array();
    $players = array();
  }

  if ($form['file_extensions']['#default_value'] == 'txt') {
    $form['file_extensions']['#default_value'] = 'mov m4v avi wmv mp4 mpg mpeg ogg ogv flv mkv';
  }

  $form['infomaniak_vod_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t("Infomaniak VOD service settings"),
    '#collapsible' => TRUE,
    '#collapsed' => !empty($folders) && !empty($widget['infomaniak_vod_folder_id']),
    '#weight' => 8,
  );

  if (empty($folders)) {
    $form['infomaniak_vod_settings']['infomaniak_vod_folder_id'] = array(
      '#type' => 'markup',
      '#value' => '<div class="message error">' . t("Please verify your credentials in the field settings (below). If they are correct, you may not have any folders configured yet on the server.") . '</div>',
    );
  }
  else {
    $form['infomaniak_vod_settings']['infomaniak_vod_folder_id'] = array(
      '#type' => 'select',
      '#title' => t("VOD folder"),
      '#description' => t("The folder to which the video must be uploaded. Note that changing this value has no impact on videos that are already uploaded. The folder defines in what formats the video will be converted and how it will be distributed to visitors."),
      '#options' => $folders,
      '#required' => TRUE,
      '#default_value' => !empty($widget['infomaniak_vod_folder_id']) ? $widget['infomaniak_vod_folder_id'] : NULL,
    );
  }

  if (empty($players)) {
    $form['infomaniak_vod_settings']['infomaniak_vod_player_id'] = array(
      '#type' => 'markup',
      '#value' => '<div class="message error">' . t("Please verify your credentials in the field settings (below). If they are correct, you may not have any players configured yet on the server.") . '</div>',
    );
  }
  else {
    $form['infomaniak_vod_settings']['infomaniak_vod_player_id'] = array(
      '#type' => 'select',
      '#title' => t("VOD player"),
      '#description' => t("The player to use for the video playback. Changing this value will affect all videos uploaded for this field instance (as opposed to changing the folder)."),
      '#options' => $players,
      '#required' => TRUE,
      '#default_value' => !empty($widget['infomaniak_vod_player_id']) ? $widget['infomaniak_vod_player_id'] : NULL,
    );
  }

  return $form;
}

/**
 * Actual implementation of hook_widget_settings(), $op = 'save'.
 */
function infomaniak_video_field_widget_settings_save($widget) {
  $filefield_settings = module_invoke('filefield', 'widget_settings', 'save', $widget);

  return array_merge(array(
    'infomaniak_vod_folder_id',
    'infomaniak_vod_player_id',
  ), $filefield_settings);
}

/**
 * Actual implementation of hook_widget().
 */
function _infomaniak_video_field_widget(&$form, &$form_state, $field, $items, $delta) {
  // We just extend Filefield.
  return filefield_widget($form, $form_state, $field, $items, $delta);
}

/**
 * Actual process callback for our custom form element.
 */
function _infomaniak_video_field_widget_process($element, $edit, &$form_state, $form) {
  $file = $element['#value'];
  $field = content_fields($element['#field_name'], $element['#type_name']);

  $element['infomaniak_vod_video_id'] = array(
    '#type' => 'hidden',
    '#value' => !empty($file['infomaniak_vod_video_id']) ? $file['infomaniak_vod_video_id'] : '',
  );

  $client = infomaniak_video_field_get_soap_client(
    $field['infomaniak_vod_login'],
    $field['infomaniak_vod_password'],
    $field['infomaniak_vod_id']
  );

  if (!$client->isAvailable()) {
    drupal_set_message(t("Unable to connect to the Infomaniak VOD service. You can still upload a video, but it might not get sent correctly to Infomaniak. If you have any doubts, wait a few minutes and try again, or contact a system administrator."), 'warning');
  }

  $element['infomaniak_vod_folder_id'] = array(
    '#type' => 'hidden',
    '#value' => !empty($file['infomaniak_vod_folder_id']) ? $file['infomaniak_vod_folder_id'] : $field['widget']['infomaniak_vod_folder_id'],
  );

  $element['infomaniak_vod_player_id'] = array(
    '#type' => 'hidden',
    '#value' => !empty($field['widget']['infomaniak_vod_player_id']) ? $field['widget']['infomaniak_vod_player_id'] : 0,
  );

  $element['infomaniak_vod_status'] = array(
    '#type' => 'hidden',
    '#value' => !empty($file['infomaniak_vod_status']) ? $file['infomaniak_vod_status'] : INFOMANIAK_VIDEO_FIELD__STATUS_PENDING,
  );

  if (!empty($element['preview'])) {
    $element['preview']['#value'] = theme('infomaniak_video_field_widget_preview', $file, $field['widget'], $field);
  }

  return $element;
}
