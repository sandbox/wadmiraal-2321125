<?php

/**
 * @file
 * Page callbacks.
 */

/**
 * VOD service callback.
 *
 * This page is called whenever an event happens on the VOD server, like
 * when a video was uploaded or converted. It is called with a POST request
 * containing information about the event.
 */
function infomaniak_video_field_vod_callback() {
  if (empty($_POST['action'])) {
    header("HTTP/1.0 400 Bad Request");
  }
  else {
    // @todo Add a setting to log these.
    watchdog('infomaniak_video_field', "Received an event notification. Data:<br /><pre>@data</pre>", array('@data' => print_r($_POST, 1)), WATCHDOG_INFO);

    switch ($_POST['action']) {
      case 'download_ok':
        infomaniak_video_field_update_status($_POST['sFileServerCode'], INFOMANIAK_VIDEO_FIELD__STATUS_CONVERSION);
        break;

      case 'encode_ok':
        infomaniak_video_field_update_status($_POST['sFileServerCode'], INFOMANIAK_VIDEO_FIELD__STATUS_READY);
        break;

      case 'encode_error':
      case 'download_error':
        infomaniak_video_field_update_status($_POST['sFileServerCode'], INFOMANIAK_VIDEO_FIELD__STATUS_ERROR);
        break;
    }

    header("HTTP/1.0 204 No Response");
  }

  // Kill the process now. We don't care about rendering pages.
  die();
}
