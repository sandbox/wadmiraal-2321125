<?php

/**
 * @file
 * Actual CCK field hook implementations, as well as field specific logic.
 */

/**
 * Actual implementation of hook_field_settings(), $op = 'form'.
 */
function infomaniak_video_field_field_settings_form($field) {
  $form = module_invoke('filefield', 'field_settings', 'form', $field);

  // We don't care about listing fields.
  $form['list_field']['#type'] = 'hidden';
  $form['list_default']['#type'] = 'hidden';

  // Our description field is slightly different, because we tie it to a
  // different formatter.
  $form['description_field']['#description'] = t("The description of the uploaded file. This will be displayed near the video player, depending on the chosen formatter.");

  $form['infomaniak_vod_id'] = array(
    '#type' => 'textfield',
    '#title' => t("Infomaniak VOD ID"),
    '#description' => t("Your account ID on the VOD server. Usually ends in '_vod'."),
    '#required' => TRUE,
    '#default_value' => isset($field['infomaniak_vod_id']) ? $field['infomaniak_vod_id'] : NULL,
  );

  $form['infomaniak_vod_login'] = array(
    '#type' => 'textfield',
    '#title' => t("Infomaniak VOD account"),
    '#description' => t("Your account login on the VOD server."),
    '#required' => TRUE,
    '#default_value' => isset($field['infomaniak_vod_login']) ? $field['infomaniak_vod_login'] : NULL,
  );

  $form['infomaniak_vod_password'] = array(
    '#type' => 'password',
    '#title' => t("Infomaniak VOD password"),
    '#description' => t("Your password on the VOD server."),
    '#required' => TRUE,
    '#attributes' => array(
      'value' => isset($field['infomaniak_vod_password']) ? $field['infomaniak_vod_password'] : NULL,
    ),
  );

  $form['infomaniak_vod_delete_video'] = array(
    '#type' => 'checkbox',
    '#title' => t("Delete video on VOD server when field is deleted"),
    '#description' => t("When a field is deleted from a node, or a node deleted, should the video be kept on the VOD server, or removed there as well ?"),
    '#default_value' => !empty($field['infomaniak_vod_delete_video']),
  );

  $form['infomaniak_vod_keep_files'] = array(
    '#type' => 'checkbox',
    '#title' => t("Keep video files on local server when processed"),
    '#description' => t("When a video is uploaded, should the original be kept on this server, or removed to save space ? Note that changing this setting has no effect on previously uploaded files that were already removed."),
    '#default_value' => isset($field['infomaniak_vod_keep_files']) ? $field['infomaniak_vod_keep_files'] : 1,
  );

  return $form;
}

/**
 * Actual implementation hook_field_settings(), $op = 'validate'.
 */
function infomaniak_video_field_field_settings_validate(&$field) {
  $result = module_invoke('filefield', 'field_settings', 'validate', $field);

  // Check connection works.
  $client = infomaniak_video_field_get_soap_client(
    $field['infomaniak_vod_login'],
    $field['infomaniak_vod_password'],
    $field['infomaniak_vod_id']
  );

  if (!$client->isAvailable()) {
    form_set_error('infomaniak_vod_id', t("Connection to the VOD server failed. Please check your credentials."));
  }

  return $result;
}

/**
 * Actual implementation hook_field_settings(), $op = 'save'.
 */
function infomaniak_video_field_field_settings_save($field) {
  $fields = module_invoke('filefield', 'field_settings', 'save', $field);

  return array_merge(array(
    'infomaniak_vod_login',
    'infomaniak_vod_password',
    'infomaniak_vod_id',
    'infomaniak_vod_delete_video',
    'infomaniak_vod_keep_files',
  ), $fields);
}

/**
 * Actual implementation of hook_field_settings(), $op = 'database_columns'.
 */
function infomaniak_video_field_field_settings_database_columns($field) {
  $columns = module_invoke('filefield', 'field_settings', 'database columns', $field);

  $columns += array(
    'infomaniak_vod_video_id' => array(
      'type' => 'varchar',
      'length' => 64,
      'not null' => TRUE,
      'default' => '',
    ),
    'infomaniak_vod_folder_id' => array(
      'type' => 'varchar',
      'length' => 64,
      'not null' => TRUE,
      'default' => '',
    ),
    'infomaniak_vod_status' => array(
      'type' => 'varchar',
      'length' => 64,
      'not null' => TRUE,
      'default' => INFOMANIAK_VIDEO_FIELD__STATUS_PENDING,
    ),
  );

  return $columns;
}

/**
 * Actual implementation of hook_field_settings(), $op = 'views data'.
 */
function infomaniak_video_field_field_settings_views_data($field) {
  $data = module_invoke('filefield', 'field_settings', 'views data', $field);

  return $data;
}

/**
 * Actual implementation of hook_field(), $op = 'load'.
 */
function infomaniak_video_field_field_load($node, $field, &$items, $teaser, $page) {
  $fields = filefield_field('load', $node, $field, $items, $teaser, $page);

  return $fields;
}

/**
 * Actual implementation of hook_field(), $op = 'insert'.
 */
function infomaniak_video_field_field_insert($node, $field, &$items, $teaser, $page) {
  $result = filefield_field('insert', $node, $field, $items, $teaser, $page);

  return $result;
}

/**
 * Actual implementation of hook_field(), $op = 'update'.
 */
function infomaniak_video_field_field_update($node, $field, &$items, $teaser, $page) {
  $result = filefield_field('update', $node, $field, $items, $teaser, $page);

  // On new revisions, old files are always maintained in the previous revision,
  // so we don't need to delete them.
  if (!empty($node->revision) || !empty($node->skip_filefield_delete)) {
    return;
  }

  // We need to know which files are still on the node, and which ones were
  // replaced (or simply removed). This has to be done here - Filefield only
  // handles its own fields.
  $fids = array();
  foreach ($items as $delta => $item) {
    // Remove items from the array if they have been deleted.
    if (empty($items[$delta]) || empty($items[$delta]['fid'])) {
      $items[$delta] = NULL;
    }
    else {
      $fids[] = $items[$delta]['fid'];
    }
  }

  // Delete items from original node.
  $old_node = node_load($node->nid);

  // If there are, figure out which ones must go.
  if (!empty($old_node->$field['field_name'])) {
    foreach ($old_node->$field['field_name'] as $old_item) {
      if (isset($old_item['fid']) && !in_array($old_item['fid'], $fids)) {
        // Delete the file.
        infomaniak_video_field_delete_file((object) $old_item, $field);
      }
    }
  }

  return $result;
}

/**
 * Actual implementation of hook_field(), $op = 'delete'.
 *
 * @todo - Revision data will not be taken care of correctly for now.
 */
function infomaniak_video_field_field_delete($node, $field, &$items, $teaser, $page) {
  foreach ($items as $item) {
    if (isset($item['fid'])) {
      infomaniak_video_field_delete_file((object) $item, $field);
    }
  }
}

/**
 * Actual implementation of hook_field(), $op = 'delete revision'.
 */
function infomaniak_video_field_field_delete_revision($node, $field, &$items, $teaser, $page) {
  foreach ($items as $delta => $item) {
    if (isset($item['fid'])) {
      // Make sure the file is not referenced elsewhere (like another revision).
      if ($references = module_invoke_all('file_references', (object) $item)) {
        $count = 0;
        foreach ($references as $key => $value) {
          $count += $value;
        }

        // If there's only 1 reference, it's the currently deleted revision.
        // We can safely delete our file.
        if ($count === 1) {
          if (infomaniak_video_field_delete_file((object) $item, $field)) {
            unset($items[$delta]);
          }
        }
      }
    }
  }
}

/**
 * Actual implementation of hook_field(), $op = 'sanitize'.
 */
function infomaniak_video_field_field_sanitize($node, $field, &$items, $teaser, $page) {
  $result = filefield_field('sanitize', $node, $field, $items, $teaser, $page);

  return $result;
}
