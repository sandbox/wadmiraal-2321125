<?php

/**
 * @file
 * Actual cron hook implementation, as well as cron specific logic.
 */

/**
 * Actual implementation of hook_cron().
 *
 * Go through the list of all our fields, and get the files that are pending.
 * Send them to the VOD service and update their status in the DB.
 */
function _infomaniak_video_field_cron() {
  $fields = infomaniak_video_field_get_field_list();

  foreach ($fields as $field) {
    $client = infomaniak_video_field_get_soap_client(
      $field['infomaniak_vod_login'],
      $field['infomaniak_vod_password'],
      $field['infomaniak_vod_id']
    );

    if ($client->isAvailable()) {
      $db_info = content_database_info($field);

      $folder_id_column = $db_info['columns']['infomaniak_vod_folder_id']['column'];
      $video_id_column = $db_info['columns']['infomaniak_vod_video_id']['column'];
      $fid_column = $db_info['columns']['fid']['column'];
      $status_column = $db_info['columns']['infomaniak_vod_status']['column'];
      $db_table = $db_info['table'];

      // Get the pending videos, and send them to Infomaniak.
      $result = db_query("
          SELECT  f.fid, f.filepath, c.nid, c.vid, c.{$folder_id_column} as folder_id, c.{$video_id_column} as video_id
            FROM  {{$db_table}} c
          }
       LEFT JOIN  {files} f ON f.fid = c.{$fid_column}
           WHERE  c.{$status_column} = '%s'
             AND  f.fid IS NOT NULL
      ", INFOMANIAK_VIDEO_FIELD__STATUS_PENDING);

      while ($row = db_fetch_array($result)) {
        if (file_exists($row['filepath']) && $url = file_create_url($row['filepath'])) {
          $video_id = !empty($row['video_id']) ? $row['video_id'] : infomaniak_video_field_get_video_id($row['filepath']);

          if ($client->upload($row['folder_id'], $url, array('filename' => $video_id))) {
            watchdog('infomaniak_video_field', "The file %file was correctly queued for upload.", array('%file' => $row['filepath']), WATCHDOG_INFO);

            db_query("
              UPDATE  {{$db_table}}
                 SET  {$status_column} = '%s', {$video_id_column} = '%s'
               WHERE  nid = %d
                 AND  vid = %d
                 AND  {$fid_column} = %d
                 AND  {$status_column} = '%s'
            ", array(
              INFOMANIAK_VIDEO_FIELD__STATUS_QUEUED,
              $video_id,
              $row['nid'],
              $row['vid'],
              $row['fid'],
              INFOMANIAK_VIDEO_FIELD__STATUS_PENDING,
            ));
          }
          else {
            watchdog('infomaniak_video_field', "An error occurred while trying to request the download of file %file. Check the error logs on the Infomaniak server.", array('%file' => $row['filepath']), WATCHDOG_ERROR);
          }
        }
        else {
          watchdog('infomaniak_video_field', "An error occurred while trying to prepare the file %file for upload. Either it doesn't exist or we have no read-access to it.", array('%file' => $row['filepath']), WATCHDOG_ERROR);
        }
      }

      // Check if videos in this field need to be "deleted".
      if (isset($field['infomaniak_vod_keep_files']) && !$field['infomaniak_vod_keep_files']) {
        // Get all videos that are ready and don't have the '.dummy' filename
        // pattern.
        $result = db_query("
            SELECT  f.fid, f.filepath
              FROM  {{$db_table}} c
            }
         LEFT JOIN  {files} f ON f.fid = c.{$fid_column}
             WHERE  c.{$status_column} = '%s'
               AND  f.fid IS NOT NULL
               AND  f.filepath NOT LIKE '%.dummy'
        ", INFOMANIAK_VIDEO_FIELD__STATUS_READY);

        while ($row = db_fetch_array($result)) {
          $new_path = $row['filepath'] . '.dummy';

          // Replace the file content with some explanatory text.
          // This will drastically reduce the file size.
          $f = fopen($row['filepath'], 'w');
          fwrite($f, "This is a dummy file, replacing the original {$row['filepath']} video file. CCK requires an actual file to be present, but as the video is now hosted by Infomaniak, it is not necessary to keep it.\n\nIf you wish to change this behavior, change the field settings. The field is '{$field['field_name']}', on the '{$field['type_name']}'' content type.");
          fclose($f);

          // Move the file itself.
          rename($row['filepath'], $new_path);

          // Update the file table.
          db_query("
            UPDATE  {files}
               SET  filepath = '%s', filemime = '%s', filesize = %d
             WHERE  fid = %d
          ", array(
            $new_path,
            'text/plain',
            filesize($new_path),
            $row['fid'],
          ));
        }
      }
    }
    else {
      watchdog('infomaniak_video_field', "Could not establish a connection with the VOD server for field %field. Skipping this field. Pending videos were not treated.", array('%field' => $field['field_name']), WATCHDOG_ERROR);
    }
  }
}
