<?php

/**
 * @file
 * SOAP client class for communicating with the Infomaniak VOD service.
 */

namespace Infomaniak\VOD;

class SoapClient {

  protected $login;
  protected $password;
  protected $vodID;
  protected $client;
  protected $isConnected;

  /**
   * Constructor.
   *
   * @param string $login
   *   The VOD account login.
   * @param string $password
   *   The VOD account password.
   * @param string $vod_id
   *   The VOD account ID.
   */
  public function __construct($login, $password, $vod_id) {
    $this->login = $login;
    $this->password = $password;
    $this->vodID = $vod_id;
  }

  /**
   * Connect to the VOD server.
   *
   * @return bool
   *   True if the connection succeeded, false otherwise.
   */
  public function connect() {
    if ($this->isConnected) {
      return TRUE;
    }

    $auth = (object) array(
      'sLogin' => $this->login,
      'sPassword' => $this->password,
      'sVod' => $this->vodID,
    );

    $this->client = new \SoapClient(
      'http://statslive.infomaniak.com/vod/api/vod_soap.wsdl',
      array(
        'trace' => 1,
        'encoding' => 'UTF-8',
      )
    );

    try {
      $this->client->__setSoapHeaders(array(
        new \SoapHeader('urn:vod_soap', 'AuthenticationHeader', $auth),
      ));

      $this->isConnected = TRUE;

      return TRUE;
    }
    catch (\Exception $e) {
      watchdog('infomaniak_video_field', "An error occurred when trying to access the VOD server: !e", array('!e' => print_r($e, 1)), WATCHDOG_ERROR);

      return FALSE;
    }
  }

  /**
   * Check the VOD server is available.
   *
   * @return bool
   *   True if the server is available, false otherwise.
   */
  public function isAvailable() {
    if ($this->isConnected || $this->connect()) {
      try {
        $this->client->ping();

        return TRUE;
      }
      catch(\Exception $e) {
        watchdog('infomaniak_video_field', "An error occurred when trying to ping the VOD server: !e", array('!e' => print_r($e, 1)), WATCHDOG_ERROR);

        return FALSE;
      }
    }

    return FALSE;
  }

  /**
   * Get the list of folders.
   *
   * @return array
   *   The list of folders, keyed by ID.
   */
  public function getFolders() {
    if ($this->isAvailable()) {
      return $this->client->getFolders();
    }
    else {
      throw new \Exception("The VOD service is not available.");
    }
  }

  /**
   * Get the list of players.
   *
   * Because this function can be called several times for a single request, the
   * result is statically cached.
   *
   * @return array
   *   The list of players, keyed by ID.
   */
  public function getPlayers() {
    static $players;

    if ($this->isAvailable()) {
      if (!isset($players)) {
        $players = $this->client->getPlayers();
      }

      return $players;
    }
    else {
      throw new \Exception("The VOD service is not available.");
    }
  }

  /**
   * Get the folder information.
   *
   * Because this function can be called several times for a single request, the
   * result is statically cached.
   *
   * @param string $folder_id
   *   The folder ID, string.
   *
   * @return array
   *   Information about the folder.
   */
  public function getFolderInfo($folder_id) {
    static $folders;

    if ($this->isAvailable()) {
      if (!isset($folders[$folder_id])) {
        $folders[$folder_id] = $this->client->getFolderInformationByPath($folder_id);
      }

      return $folders[$folder_id];
    }
    else {
      throw new \Exception("The VOD service is not available.");
    }
  }

  /**
   * Get the player iFrame.
   *
   * @param string $folder_id
   *   The folder ID the video is in.
   * @param string $video_id
   *   The video ID.
   * @param int $player_id
   *   The player ID.
   * @param int $width
   *   (optional) The player width. This will override the width that was set
   *   for the player on the VOD server.
   * @param int $height
   *   (optional) The player height. This will override the height that was set
   *   for the player on the VOD server.
   *
   * @return string
   *   The HTML for the iframe.
   */
  public function getPlayerIFrame($folder_id, $video_id, $player_id, $width = NULL, $height = NULL) {
    if ($this->isAvailable()) {
      // Because those morons at Infomaniak sometimes require the folder ID
      // (string) and sometimes the folder code (int), we need to fetch that
      // information first. Depressing, stupid, incompetent are just a few words
      // that come to mind when seeing such bulls*&/%. Why not use the same ID
      // (be it numerical or string-based) everywhere ???
      $folder_info = $this->getFolderInfo($folder_id);

      // Another inhuman stupidity is the requirement to provide a width and
      // height for the player. Even though this is ALREADY CONFIGURED on the
      // player itself. Having these as optional parameters would make sense
      // (we might want to override them), but having them as REQUIRED is just
      // stupid. And, as there's no way to get the information for a single
      // player, we get the full list, fetch the one we need from it, check
      // the width and height and put it out. Simple, but completely useless.
      foreach ($this->getPlayers() as $player_info) {
        if ($player_info['iPlayerCode'] == $player_id) {
          break;
        }
      }

      // And now, we can - hurray ! - finally get the iframe. Partaayy !!
      return $this->client->getIFrameImplementation(
        $folder_info['iFolderCode'],
        $video_id,
        isset($width) ? $width : $player_info['iWidth'],
        isset($height) ? $height : $player_info['iHeight'],
        $player_id
      );
    }
    else {
      throw new \Exception("The VOD service is not available.");
    }
  }

  /**
   * Send an upload request.
   *
   * @param string $folder_id
   *   The folder ID to upload to.
   * @param string $url
   *   The URL at which the video file can be found.
   * @param array $options
   *   The options for the upload, like filename or username and password
   *   if the URL requires an authentication.
   *
   * @return bool
   *   The webservice response, either true (payload accepted) or false.
   */
  public function upload($folder_id, $url, $options = array()) {
    if ($this->isAvailable()) {
      return $this->client->importFromUrl($folder_id, $url, $options);
    }
    else {
      throw new \Exception("The VOD service is not available.");
    }
  }

  /**
   * Delete the file.
   *
   * @param string $folder_id
   *   The folder ID the video is in.
   * @param string $video_id
   *   The video ID.
   */
  public function delete($folder_id, $video_id) {
    if ($this->isAvailable()) {
      // Because those morons at Infomaniak sometimes require the folder ID
      // (string) and sometimes the folder code (int), we need to fetch that
      // information first. Depressing, stupid, incompetent are just a few words
      // that come to mind when seeing such bulls*&/%. Why not use the same ID
      // (be it numerical or string-based) everywhere ???
      $folder_info = $this->getFolderInfo($folder_id);

      try {
        // This has some risks - if the file was already deleted by hand on the
        // VOD server, this request will fail and give our users a nice WSOD.
        // Catch any errors - we just take it it worked and the video was
        // removed.
        $this->client->deleteVideo($folder_info['iFolderCode'], $video_id);
      }
      catch (\Exception $e) {}
    }
    else {
      throw new \Exception("The VOD service is not available.");
    }
  }
}
